@extends('layouts.guru')

@section('content')
    <!-- Default Basic Forms Start -->
    <div class="pd-20 card-box mt-3">
        <form action="{{ route('guru.paket.store') }}" method="POST">
            @csrf
            <div class="clearfix">
                <div class="pull-left">
                    <h4 class="text-blue h4">Create Package Forms</h4>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
            <div class="form-group row mt-4">
                <label class="col-sm-12 col-md-2 col-form-label">Package Name</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="paket" type="paket" placeholder="Unique">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Package Password</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="password" placeholder="Enter Password">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-12 col-md-2 col-form-label">Quiz Duration (Minutes)</label>
                <div class="col-sm-12 col-md-10">
                    <input class="form-control" name="durasi" placeholder="120" type="durasi">
                </div>
            </div>
        </form>
    </div>
@endsection