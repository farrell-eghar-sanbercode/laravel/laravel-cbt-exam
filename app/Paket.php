<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    protected $table = "paket";

    protected $guarded = [];

    // Eloquent Relationship one-to-many
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    // Eloquent Relationship one-to-many
    public function soal() {
        return $this->hasMany('App\Soal', 'paket_id');
    }

    // Eloquent Relationship many-to-many
    public function ujian() {
        return $this->belongsToMany('App\User', 'ujian', 'user_id', 'paket_id');
    }
}
